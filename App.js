/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react'
import {
  Dimensions, TouchableOpacity,
  StyleSheet,
  Text,
  View
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import { TfImageRecognition } from 'react-native-tensorflow'
import ViewShot, { captureScreen } from 'react-native-view-shot'
import RNFS from 'react-native-fs'

const tmpFile = RNFS.DocumentDirectoryPath + '/tmp.jpg'

const screenWidth = Dimensions.get('window').width
const screenHeight = Dimensions.get('window').height

const tfImageRecognition = new TfImageRecognition({
  model: require('./assets/tensorflow_inception_graph.pb'),
  labels: require('./assets/tensorflow_labels.txt'),
  imageMean: 117, // Optional, defaults to 117
  imageStd: 1 // Optional, defaults to 1
})

export default class App extends Component {
  state = {
    guess: 'guess',
    confidence: 0,
  }
  async componentDidMount() {
    const results = await tfImageRecognition.recognize({
      image: require('./assets/dumbbell.jpg'),
      inputName: "input", //Optional, defaults to "input"
      inputSize: 224, //Optional, defaults to 224
      outputName: "output", //Optional, defaults to "output"
      maxResults: 10, //Optional, defaults to 3
      threshold: 0.1, //Optional, defaults to 0.1
    })
    results.forEach(result =>
      console.log(
        result.id, // Id of the result
        result.name, // Name of the result
        result.confidence // Confidence value between 0 - 1
      )
    )
    
    
  }
  recognize = async (uri) => {
    return tfImageRecognition.recognize({
      // image: uri.replace('file://', ''),
      image: tmpFile,
      // image: require('./assets/dumbbell.jpg'),
      inputName: "input", //Optional, defaults to "input"
      inputSize: 224, //Optional, defaults to 224
      outputName: "output", //Optional, defaults to "output"
      maxResults: 1, //Optional, defaults to 3
      threshold: 0.1, //Optional, defaults to 0.1
    })
  }
  takeView = async () => {
    console.log('take view...')
    if (this.refs.viewShot) {
      this.refs.viewShot.capture() // for iphoneX issue      
      .then(
        async uri => {
          try {
            // console.log("captureScreen snapshot uri : ", uri)
            await RNFS.writeFile(tmpFile, uri, 'base64')
            const results = await this.recognize(uri)
            console.log(results)
            if (results && results.length > 0) {
              this.setState({
                guess: results[0].name,
                confidence: results[0].confidence
              })
            }
            
          } catch (err) {
            console.log(err)
          }
        },
        error => {
          console.error("Oops, snapshot failed", error)
          this.snapCaptured = true
        }
      )
    }
  }
  start = () => {
    this.interval = setInterval(() => {
      this.takeView()
    }, 2000)
  }
  stop = () => {
    if (this.interval) {
      clearInterval(this.interval)
    }
  }
  async componentWillUnMount() {
    await tfImageRecognition.close()
  }
  render() {
    return (
      <ViewShot
        ref='viewShot'
        options={{ format: 'jpg', quality: 0.5, result: 'base64' }}
        style={{ width: screenWidth, height: screenHeight }}
      >
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{ width: screenWidth, height: screenHeight }}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          permissionDialogTitle={'Permission to use camera'}
          permissionDialogMessage={'We need your permission to use your camera phone'}
        />
        <TouchableOpacity
            onPress={this.start}
            style = {{ position: 'absolute', left: 0, backgroundColor: '#fff' }}
        >
            <Text style={{fontSize: 14}}> start </Text>
        </TouchableOpacity>
        <TouchableOpacity
            onPress={this.stop}
            style = {{ position: 'absolute', right: 0, backgroundColor: '#fff' }}
        >
            <Text style={{fontSize: 14}}> stop </Text>
        </TouchableOpacity>
        <View style={{ position: 'absolute', top: 0, left: 100, backgroundColor: '#fff' }}>
          <Text style={{ color: 'blue', fontSize: 18 }}>{this.state.guess}</Text>
        </View>
      </ViewShot>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
